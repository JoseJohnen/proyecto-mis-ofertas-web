﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using LibreriaMisOfertas;
using DatosMisOfertas;
using System.Data;

namespace InterfazWebService
{
    public class InterfazWebService
    {
        //Aquí se agregarán todos los métodos que deban connectar con la base de datos que sean usados por la aplicación cliente,
        //vean en el proyecto "webMisOfertasResponsive" en la carpeta "WebService" en donde guardaré la WebForm que recibirá las instrucciones
        //de la aplicación cliente y en donde utilizaremos esta librería.
        public bool isConnected(Consumidor c)
        {
            conexionOracle conexion = new conexionOracle();
            OracleConnection con = new OracleConnection(conexion.getConnectionString);
            con.Open();
            OracleCommand cmd =new OracleCommand("select * from misofertasdb.libro where id_libro=:libro", con);
            cmd.CommandType = CommandType.Text;
            //este es un select (revisa el code mio si tienes dudas)
            cmd.Parameters.Add(":libro", c.correoConsumidor);
            //este es para insert (revisa el code mio si tienes dudas)
            cmd.Parameters.Add("libro",OracleDbType.Varchar2).Value=c.correoConsumidor;

            //obtener datos desde el select con execute reader
            OracleDataReader reader = cmd.ExecuteReader();
            c.contrasenaConsumidor = reader["contrasena_usuario"].ToString();
            

            //esto es solo para que te dija que existe o no existe
            cmd.ExecuteNonQuery();
            return true;
        }

        
    }
}
